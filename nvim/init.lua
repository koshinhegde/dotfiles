require "kosh.options"
require "kosh.keymaps"
require "kosh.plugins"


-- Fold
require("fold-preview").setup()
require("pretty-fold").setup({
    sections = {
        left = {
            'content',
        },
        right = {
            ' ', 'number_of_folded_lines',
            function(config) return config.fill_char:rep(3) end
        }
    },
    fill_char = " "
})
require("nvim-treesitter").setup {
    highlight = {
        enable = true,
    },
    endent = { enable = true },
    fold = { enable = true },
}


-- Scratchpad setup
file_path = vim.uri_to_fname(vim.uri_from_bufnr(0))

if (file_path == "/home/kosh/.backup/notes.txt") then
    vim.api.nvim_set_keymap("n", "ZZ", ":w<CR>:!i3-msg \"[instance=\"notes\"] move scratchpad\"<CR><CR>", {noremap = true})
end


-- MISC
vim.cmd "colorscheme onedarker"

require("lualine").setup {
    theme = "palenight"
}

-- AS
require("autosave").setup({
    enabled = true
})


-- Git signs
require('gitsigns').setup()

-- tabline
require("bufferline").setup{}
require('nvim-web-devicons').setup{}

-- Jupyter
