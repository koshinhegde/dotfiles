-- Numbers
vim.opt.relativenumber = true			-- Relative numbers
vim.opt.number = true                   -- Normal numbers
vim.opt.scrolloff = 9999

-- Tabs
vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.autoindent = true
vim.opt.expandtab = true			    -- Converts tabs to spaces

vim.opt.cb = unnamedplus				-- Clipboard
vim.cmd [[
let g:clipboard = {
    \   'copy': {
    \       '+': ['wl-copy', '--trim-newline'],
    \       '*': ['wl-copy', '--trim-newline'],
    \   },
    \   'paste': {
    \       '+': ['wl-paste', '--no-newline'],
    \       '*': ['wl-paste', '--no-newline'],
    \   },
    \ }
]]
vim.opt.mouse = "a"						-- Mouse enable
vim.opt.fileencoding = "utf-8"			-- File encoding
vim.opt.showtabline = 0					-- The top tabs
vim.opt.cursorline = true               -- Underline
vim.opt.termguicolors = true
vim.opt.undofile = true                 -- UNLIMITED UNDOS!!!
vim.opt.ignorecase = true
vim.opt.smartcase = false

-- Folds
vim.opt.foldmethod = "expr"
vim.cmd "set foldexpr=nvim_treesitter#foldexpr()"
--vim.cmd([[
--    set foldexpr=GetPotionFold(v:lnum)
--
--    function! IndentLevel(lnum)
--        return indent(a:lnum) / &shiftwidth
--    endfunction
--
--    function! GetPotionFold(lnum)
--        if getline(a:lnum + 1) =~? "^\s*\{\s*$"
--            return ">"..IndentLevel(a:lnum + 2)
--        elseif getline(a:lnum) =~? "^\s*\}\s*$"
--            return "<"..IndentLevel(a:lnum - 1)
--        endif
--        return "="
--    endfunction
--]])

vim.opt.foldlevel = 0

--vim.g.jukit_output_new_os_window = 1
vim.cmd([[
    let g:jukit_layout = {
        \'split': 'vertical',
        \'p1': 0.6, 
        \'val': [
            \'file_content',
            \{
                \'split': 'vertical',
                \'p1': 0.6,
                \'val': ['output', 'output_history']
            \}
        \]
    \}
]])
vim.g.jukit_terminal = "kitty"
vim.g.jukit_output_new_os_window = 1
vim.g.jukit_convert_overwrite_default = 1
vim.g.jukit_convert_open_default = 0
