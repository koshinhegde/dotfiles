local packer_settings = {
  git = {
    clone_timeout = ( 99999 ),
  },
}

return require("packer").startup(function(use)
    use { 'anuvyklack/fold-preview.nvim', requires = 'anuvyklack/keymap-amend.nvim'}
    use "anuvyklack/pretty-fold.nvim"
    use "nvim-treesitter/nvim-treesitter"

    use { "neoclide/coc.nvim", branch="release" }

    use {
        "nvim-lualine/lualine.nvim",
        requires = { "kyazdani42/nvim-web-devicons", opt = true }
    }

    use "LunarVim/Onedarker.nvim"

    use {
        "nvim-telescope/telescope.nvim",
        tag = '0.1.2',
        requires = { {'nvim-lua/plenary.nvim'}, {"nvim-tree/nvim-web-devicons"} }
    }

    use {"sar/AutoSave.nvim"}

    use "habamax/vim-godot"

    use({
        "iamcco/markdown-preview.nvim",
        run = function() vim.fn["mkdp#util#install"]() end,
    })

    use "lewis6991/gitsigns.nvim"

    use {
      'VonHeikemen/fine-cmdline.nvim',
      requires = {
        {'MunifTanjim/nui.nvim'}
      }
    }

    use {'akinsho/bufferline.nvim', tag = "*", requires = 'nvim-tree/nvim-web-devicons'}

    use {"nvim-tree/nvim-web-devicons"}

    use "terrastruct/d2-vim"
    use({
      "WilsonOh/emoji_picker-nvim",
      config = function()
        require("emoji_picker").setup()
      end,
    })
    use "adelarsq/vim-devicons-emoji"

    use { 'dccsillag/magma-nvim', run = ':UpdateRemotePlugins' }
    use {"meatballs/notebook.nvim"}
    use "luk400/vim-jukit"

    use({
        "kylechui/nvim-surround",
        tag = "*", -- Use for stability; omit to use `main` branch for the latest features
        config = function()
            require("nvim-surround").setup({
                -- Configuration here, or leave empty to use defaults
            })
        end
    })
end)
