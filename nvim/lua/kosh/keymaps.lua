local set_keymap = vim.api.nvim_set_keymap
local opts = { noremap = true, silent = true }
vim.g.mapleader = " "


set_keymap("n", "<C-s>", ":w<CR>", opts)  -- Save

-- Autocomplete brackets and quotes
set_keymap("i", "\"", "\"\"<left>", opts)
set_keymap("i", "'", "''<left>", opts)
set_keymap("i", "(", "()<left>", opts)
set_keymap("i", "[", "[]<left>", opts)
set_keymap("i", "{", "{}<left>", opts)
set_keymap("i", "<", "<><left>", opts)

-- Coc config
vim.cmd[[ inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
\: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>" ]]
set_keymap("n", "<leader>a", "<Plug>(coc-codeaction-selected)l", opts)

-- Black-py setup
set_keymap("n", "<leader>=", "m`gg=G``", opts)

-- Split Window config
set_keymap("n", "<C-h>", "<C-w>h", opts)
set_keymap("n", "<C-j>", "<C-w>j", opts)
set_keymap("n", "<C-k>", "<C-w>k", opts)
set_keymap("n", "<C-l>", "<C-w>l", opts)

-- Tabs
--set_keymap("n", "<S-l>", ":tabn<CR>", opts)
--set_keymap("n", "<S-h>", ":tabp<CR>", opts)

-- Telescope
local builtin = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})

-- Buffers
set_keymap("n", "<S-l>", ":bnext<CR>", opts)
set_keymap("n", "<S-h>", ":bprev<CR>", opts)

-- Folds
set_keymap("n", "<leader>zo", "zo", opts)
set_keymap("n", "<leader>zO", "zR", opts)
set_keymap("n", "<leader>zc", "zc", opts)
set_keymap("n", "<leader>zC", "zM", opts)
set_keymap("n", "<leader>za", "za", opts)
set_keymap("n", "<leader>zA", "zA", opts)

-- cmdline
set_keymap('n', ':', '<cmd>FineCmdline<CR>', opts)
set_keymap('n', '/', '<cmd>FineCmdline<CR>/', opts)

--set_keymap("n", "<leader>nbo")
set_keymap("n", "<leader>ml", ":MagmaEvaluateLine<CR>", opts)
set_keymap("v", "<leader>mv", ":<BS><BS><BS><BS><BS>MagmaEvaluateVisual<CR>", opts)
set_keymap("n", "<leader>mc", ":MagmaReevaluateCell<CR>", opts)
set_keymap("n", "<leader>mi", ":MagmaInterrupt<CR>", opts)
set_keymap("n", "<leader>mI", ":MagmaInit python<CR>", opts)


set_keymap("n", "<S-j>", ":call jukit#cells#jump_to_next_cell()<cr>", opts)
set_keymap("n", "<S-k>", ":call jukit#cells#jump_to_previous_cell()<cr>", opts)
set_keymap("n", "<C-S-c>", ":call system('wl-copy', @\")<CR>", opts)
--nnoremap <C-S-C> :call system("wl-copy", @")<CR>
